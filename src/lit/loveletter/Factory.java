package lit.loveletter;

import oop.lit.model.GameFactory;
import oop.lit.model.GameModel;
import oop.lit.view.ViewRequests;

/**
 * The factory for a LoveLetter game.
 */
public class Factory implements GameFactory {
    @Override
    public GameModel getGame(final ViewRequests view) {
        return new GameModel(new LLGame(view));
    }
    @Override
    public String getName() {
        return "Love letter";
    }
}
