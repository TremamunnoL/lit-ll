package lit.loveletter;

import java.util.List;

import oop.lit.model.Action;
import oop.lit.model.PlayerModel;
import oop.lit.model.elements.BoardElement;
import oop.lit.model.groups.AbstractBoard;

/**
 * A love letter game board.
 */
public class LLBoard extends AbstractBoard<BoardElement> {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Override
    public List<Action> getSelectedActions(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return this.getSingleSelectionActions(playingPlayer, turnPlayers);
    }

}
