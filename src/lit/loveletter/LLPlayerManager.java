package lit.loveletter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import lit.loveletter.elements.LLCard;
import oop.lit.model.PlayerModel;
import oop.lit.model.game.PlayerManagerImpl;
import oop.lit.model.groups.ElementGroup;
import oop.lit.model.groups.ElementGroupImpl;
import oop.lit.view.ViewRequests;

/**
 * A player manager for a LoveLetter Game.
 * Players added to this manager will always have an hand associated.
 */
public class LLPlayerManager extends PlayerManagerImpl<LLPlayer, LLPlayerHand> {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private final Map<PlayerModel, ElementGroup<LLCard>> discardedGroups = new HashMap<>();
    private final ElementGroup<LLCard> deck;
    private final ViewRequests view;
    private int currentPlayer;

    /**
     * @param deck
     *      this game deck.
     * @param view
     *      this game view.
     */
    public LLPlayerManager(final ElementGroup<LLCard> deck, final ViewRequests view) {
        this.deck = deck;
        this.view = view;
    }

    /**
     * @param pNumber
     *      the number of players. Can only be 2, 3 or 4;
     * @throws IllegalArgumentException
     *      if the player number is invalid.
     */
    public void initPlayers(final int pNumber) {
        if (!this.getPlayers().isEmpty()) {
            throw new IllegalStateException();
        }
        if (pNumber < 2 || pNumber > 4) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < pNumber; i++) {
            this.addPlayer(new LLPlayer("P" + i));
        }
        this.initialDraw();
    }

    /**
     * Resets the player manager for a new round.
     */
    public void reset() {
        this.getPlayers().forEach(LLPlayer::reset);
        this.getAllPlayersHand().values().forEach(LLPlayerHand::clear);
        this.discardedGroups.values().forEach(ElementGroup<LLCard>::clear);
        this.initialDraw();
    }

    private void initialDraw() {
        this.currentPlayer = 0;
        this.getPlayers().forEach(this::drawTo);
        this.setActivePlayer(this.getPlayers().get(this.currentPlayer));
        this.drawTo(this.getPlayers().get(this.currentPlayer));
    }

    @Override
    public boolean addPlayer(final LLPlayer player) {
        final boolean res = super.addPlayer(player);
        if (res) {
            this.addPlayerHand(player, new LLPlayerHand(player));
            this.discardedGroups.put(player, new ElementGroupImpl<>(Optional.of(player.getName() + " discarded")));
        }
        return res;
    }

    @Override
    public boolean removePlayer(final PlayerModel player) {
        final boolean res = super.removePlayer(player);
        if (res) {
            this.discardedGroups.remove(player);
        }
        return res;
    }

    /**
     * @param player
     *            a player.
     * @return 
     *      an optional of the group containing all the cards the provided
     *      player discarded, or an empty optional if the player is not
     *      contained in this manager.
     */
    public Optional<ElementGroup<LLCard>> getDiscardedGroup(final PlayerModel player) {
        return Optional.ofNullable(this.discardedGroups.get(player));
    }

    /**
     * @return
     *      a list of players that can be targeted, excluding the playing player (if present).
     */
    public Set<LLPlayer> getTargetablePlayersEx() {
        if (!this.getPlayingPlayer().isPresent()) {
            return getAllTargetablePlayers();
        }
        return getTargetablePlayers(this.getPlayingPlayer().get());
    }
    /**
     * @return
     *      a list of all players that can be targeted.
     */
    public Set<LLPlayer> getAllTargetablePlayers() {
        return this.getTargetablePlayers(null);
    }
    private Set<LLPlayer> getTargetablePlayers(final LLPlayer exclude) {
        return this.getPlayers().stream()
                .filter(player -> player.canPlay() && !player.isPlayerProtected() && !player.equals(exclude))
                .collect(Collectors.toSet());
    }

    /**
     * @return
     *      a set containing all groups of discarded cards.
     */
    public Set<ElementGroup<LLCard>> getAllDiscardedGroups() {
        return new HashSet<>(this.discardedGroups.values());
    }
    /**
     * Updates the active players.
     */
    public void nextTurn() {
        if (this.getPlayers().isEmpty()) {
            throw new IllegalStateException();
        }
        final List<LLPlayer> remainingPlayers = this.getPlayers().stream().filter(LLPlayer::canPlay).collect(Collectors.toList());
        if (remainingPlayers.size() == 1) {
            view.displayMessage("Player " + remainingPlayers.get(0).getName() + "has won");
            remainingPlayers.get(0).knockOut();
        } else if (deck.getElements().isEmpty()) {
            final Map<LLPlayer, Integer> strenghtMap = remainingPlayers.stream().collect(Collectors.toMap(Function.identity(), p -> {
                return this.getPlayerHand(p).get().getElements().get(0).getType().getStrength();
            }));
            if (new ArrayList<>(strenghtMap.values()).size() != new HashSet<>(strenghtMap.values()).size()) {
                view.displayMessage("The game ended with a tie");
            } else {
                view.displayMessage("Player " + strenghtMap.entrySet().stream()
                    .max((entry, entry2) -> Integer.compare(entry.getValue(), entry2.getValue())).get().getKey()
                    .getName() + " wins");
                remainingPlayers.forEach(LLPlayer::knockOut);
            }
        } else {
            this.cleanOut();
            do {
                this.currentPlayer++;
                this.currentPlayer %= this.getPlayers().size();
            } while (!this.getPlayers().get(this.currentPlayer).canPlay());
            final LLPlayer player = this.getPlayers().get(this.currentPlayer);
            this.setActivePlayer(player);
            player.setPlayerProtected(false);
            this.drawTo(player);
        }
    }

    private void cleanOut() {
        this.getPlayers().stream().filter(player -> !player.canPlay()).forEach(player -> {
            final LLPlayerHand pHand = this.getPlayerHand(player).get();
            if (!pHand.getElements().isEmpty()) {
                final LLCard element = pHand.getElements().get(0);
                pHand.removeElement(element);
                element.discarded();
                this.getDiscardedGroup(player).get().addElement(element);
            }
        });
    }

    private void drawTo(final LLPlayer player) {
        final LLCard deckCard = this.deck.getElements().get(0);
        this.deck.removeElement(deckCard);
        this.getPlayerHand(player).get().addElement(deckCard);
        deckCard.setHoldingPlayer(Optional.of(player));
    }
}
