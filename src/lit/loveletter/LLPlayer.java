package lit.loveletter;

import oop.lit.model.game.Player;

/**
 * A Love letter player.
 */
public class LLPlayer extends Player {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private boolean canPlayValue = true;
    private boolean isProtected;
    /**
     * @param name
     *      the player name.
     */
    public LLPlayer(final String name) {
        super(name);
    }

    /**
     * Knocks the player out of the round. This player can't play anymore.
     */
    public void knockOut() {
        this.canPlayValue = false;
    }

    /**
     * Reset the player to its initial state.
     */
    public void reset() {
        this.canPlayValue = true;
        this.isProtected = false;
    }

    /**
     * @return
     *      if the player can play.
     */
    public boolean canPlay() {
        return this.canPlayValue;
    }

    /**
     * @return if the is protected
     */
    public boolean isPlayerProtected() {
        return isProtected;
    }

    /**
     * @param isProtected
     *      if the player is to be set as protected.
     */
    public void setPlayerProtected(final boolean isProtected) {
        this.isProtected = isProtected;
    }
}
