package lit.loveletter;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

/**
 * A class used to get Images for LoveLetterGame.
 */
public class LLImageManager {
    /**
     * An enum used to identify images different from the card front.
     */
    public enum ImageType {
        //CHECKSTYLE:OFF
        BACK, EMPTY_GROUP, RULES1, RULES2; //CHECKSTYLE:ON
    }
    /**
     * An enum used to change what images are displayed.
     */
    public enum Skin {
        //CHECKSTYLE:OFF
        DEFAULT("defaultSkin"); // CHECKSTYLE:ON
        private final String folder;
        Skin(final String folder) {
            this.folder = folder;
        }
    }
    private static final String EXTENSION = ".png";
    private final Map<LLCardType, BufferedImage> cardMap = new HashMap<>();
    private final Map<ImageType, BufferedImage> otherMap = new HashMap<>();
    private final Skin skin;

    /**
     * @param skin
     *      the used skin.
     */
    public LLImageManager(final Skin skin) {
        this.skin = skin;
        this.cardMap.put(LLCardType.GUARD, getImage("1"));
        this.cardMap.put(LLCardType.PRIEST, getImage("2"));
        this.cardMap.put(LLCardType.BARON, getImage("3"));
        this.cardMap.put(LLCardType.HANDMAID, getImage("4"));
        this.cardMap.put(LLCardType.PRINCE, getImage("5"));
        this.cardMap.put(LLCardType.KING, getImage("6"));
        this.cardMap.put(LLCardType.COUNTESS, getImage("7"));
        this.cardMap.put(LLCardType.PRINCESS, getImage("8"));
        this.otherMap.put(ImageType.BACK, getImage("B"));
        this.otherMap.put(ImageType.EMPTY_GROUP, getImage("E"));
        this.otherMap.put(ImageType.RULES1, getImage("R1"));
        this.otherMap.put(ImageType.RULES2, getImage("R2"));
    }

    /**
     * @param cardType
     *      the type of card you want to get the image for.
     * @return
     *      the image.
     */
    public BufferedImage getCardImage(final LLCardType cardType) {
        return this.cardMap.get(cardType);
    }

    /**
     * @param type
     *      the type of image you want to get.
     * @return
     *      the image.
     */
    public BufferedImage getOtherImage(final ImageType type) {
        return this.otherMap.get(type);
    }

    private BufferedImage getImage(final String name) {
        try {
            return ImageIO.read(this.getClass().getResourceAsStream("/" + skin.folder + "/" + name + EXTENSION));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
