package lit.loveletter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import lit.loveletter.LLImageManager.Skin;
import lit.loveletter.elements.LLCard;
import lit.loveletter.elements.LLCardActionFactory;
import lit.loveletter.elements.LLDeck;
import lit.loveletter.elements.LLDiscarded;
import lit.loveletter.elements.LLRulesCard;
import oop.lit.model.Action;
import oop.lit.model.actions.AbstractAction;
import oop.lit.model.actions.Actions;
import oop.lit.model.game.Game;
import oop.lit.model.groups.ElementGroupImpl;
import oop.lit.model.groups.GroupViewer;
import oop.lit.util.CollectionsUtils;
import oop.lit.util.IllegalInputException;
import oop.lit.util.InputRequest;
import oop.lit.util.InputRequestsFactory;
import oop.lit.util.ObservableImpl;
import oop.lit.view.ViewRequests;

/**
 * Main class for a love letter game.
 */
public class LLGame extends ObservableImpl implements Game<LLBoard, LLPlayerManager> {
    private final LLBoard board = new LLBoard();
    private final ElementGroupImpl<LLCard> deck = new ElementGroupImpl<LLCard>(Optional.of("Deck"));
    private final LLPlayerManager pManager;
    private final GroupViewer gViewer = new GroupViewer();
    private final transient List<Action> actions = new ArrayList<>();

    /**
     * @param view
     *      this game view.
     */
    public LLGame(final ViewRequests view) {
        this.pManager = new LLPlayerManager(deck, view);
        this.pManager.attach(() -> this.notifyObservers());

        final LLImageManager images = new LLImageManager(Skin.DEFAULT);
        final LLCardActionFactory actionFactory = new LLCardActionFactory(pManager, view, deck);

        this.initDeck(images, actionFactory);

        this.pManager.initPlayers(4);

        this.board.addElement(new LLDeck(deck, images, view));
        this.board.addElement(new LLRulesCard(images));
        this.pManager.getAllDiscardedGroups().forEach(group -> {
            this.board.addElement(new LLDiscarded(group, images, this.gViewer));
        });

        this.actions.add(new AbstractAction("Change player") {
            private Optional<InputRequest<LLPlayer>> playerIR = Optional.empty();
            @Override
            public boolean canBePerformed() {
                return !pManager.getPlayers().isEmpty();
            }
            @Override
            public List<InputRequest<?>> getRequests(final InputRequestsFactory irFactory) {
                this.playerIR = Optional.of(irFactory.getChoiceInputRequest("Player",
                        CollectionsUtils.mapWithDifferentStrings(pManager.getPlayers(), LLPlayer::getName),
                        Optional.empty()));
                return Arrays.asList(playerIR.get());
            }
            @Override
            public void perform() throws IllegalInputException {
                pManager.setPlayingPlayer(Actions.checkPresentAndGet(this.playerIR));
                gViewer.stopShowingAll();
            }
        });
        this.actions.add(new AbstractAction("See hand") {
            @Override
            public boolean canBePerformed() {
                return pManager.getPlayingPlayer().isPresent();
            }
            @Override
            public void perform() throws IllegalInputException {
                this.checkPerformable();
                gViewer.showSelectable(pManager.getPlayerHand(pManager.getPlayingPlayer().get()).get());
            }
        });
        this.actions.add(new AbstractAction("Reset game") {
            @Override
            public void perform() throws IllegalInputException {
                reset(images, actionFactory);
            }
        });
    }

    private void reset(final LLImageManager images, final LLCardActionFactory actionFactory) {
        this.initDeck(images, actionFactory);
        this.gViewer.stopShowingAll();
        this.pManager.reset();
    }

    private void initDeck(final LLImageManager images, final LLCardActionFactory actionFactory) {
        final List<LLCard> cards = new ArrayList<>();
        Arrays.asList(LLCardType.values()).stream().forEach(cardType -> {
            for (int i = 0; i < cardType.getCopies(); i++) {
                cards.add(new LLCard(cardType, images, actionFactory));
            }
        });
        Collections.shuffle(cards);
        cards.remove(0);
        deck.clear();
        cards.forEach(deck::addElement);
    }

    @Override
    public LLBoard getBoard() {
        return this.board;
    }

    @Override
    public LLPlayerManager getPlayerManager() {
        return this.pManager;
    }

    @Override
    public GroupViewer getGroupViewer() {
        return this.gViewer;
    }

    @Override
    public List<Action> getActions() {
        return new ArrayList<>(this.actions);
    }

    @Override
    public void setView(final ViewRequests view) {
    }
}
