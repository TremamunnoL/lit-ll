package lit.loveletter;

/**
 * An enum containing informations about love letter card types.
 */
public enum LLCardType {
    //CHECKSTYLE:OFF
    GUARD("Guard", 5, 1),
    PRIEST("Priest", 2, 2),
    BARON("Baron", 2, 3),
    HANDMAID("Handmaid", 2, 4),
    PRINCE("prince", 2, 5),
    KING("King", 1, 6),
    COUNTESS("Countess", 1, 7),
    PRINCESS("Princess", 1, 8);
    //CHECKSTYLE:ON

    private final String readableName;
    private final int copies;
    private final int strength;
    LLCardType(final String readableName, final int copies, final int strength) {
        this.readableName = readableName;
        this.copies = copies;
        this.strength = strength;
    }
    /**
     * @return the readableName
     */
    public String getReadableName() {
        return readableName;
    }
    /**
     * @return the copies
     */
    public int getCopies() {
        return copies;
    }
    /**
     * @return the card strength
     */
    public int getStrength() {
        return strength;
    }
}
