package lit.loveletter.elements;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import lit.loveletter.LLImageManager;
import lit.loveletter.LLImageManager.ImageType;
import oop.lit.model.Action;
import oop.lit.model.PlayerModel;
import oop.lit.model.actions.AbstractAction;
import oop.lit.model.elements.AbstractBoardElement;
import oop.lit.model.groups.ElementGroup;
import oop.lit.util.IllegalInputException;
import oop.lit.view.ViewRequests;

/**
 * A board element used to show the deck.
 */
public class LLDeck  extends AbstractBoardElement {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private final transient BufferedImage backImage;
    private final transient BufferedImage emptyImage;
    private final transient Action showNumber;
    private final ElementGroup<LLCard> group; 

    /**
     * @param group
     *      the group to be visualized.
     * @param imageManager
     *      the game imageManager.
     * @param view
     *      this game view.
     */
    public LLDeck(final ElementGroup<LLCard> group, final LLImageManager imageManager, final ViewRequests view) {
        super(group.getGroupName(), Optional.empty(), Optional.empty(), Optional.empty());
        this.group = group;
        this.group.attach(() -> this.notifyObservers());
        this.backImage = imageManager.getOtherImage(ImageType.BACK);
        this.emptyImage = imageManager.getOtherImage(ImageType.EMPTY_GROUP);
        this.showNumber = new AbstractAction("Show number of cards left") {
            @Override
            public void perform() throws IllegalInputException {
                view.displayMessage(getName().orElse("this") + " has " + group.getElements().size() + " cards left");
            }
        };
    }

    @Override
    public boolean canMove(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return true;
    }
    @Override
    public boolean canScale(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return true;
    }
    @Override
    public boolean canRotate(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return true;
    }

    @Override
    public List<Action> getActions(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return Arrays.asList(showNumber);
    }

    @Override
    public Optional<Action> getMainAction(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return Optional.of(showNumber);
    }

    @Override
    public Optional<BufferedImage> getImage() {
        if (this.group.getElements().isEmpty()) {
            return Optional.of(this.emptyImage);
        } else {
            return Optional.of(this.backImage);
        }
    }

}
