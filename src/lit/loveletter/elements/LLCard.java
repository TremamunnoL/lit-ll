package lit.loveletter.elements;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import lit.loveletter.LLCardType;
import lit.loveletter.LLImageManager;
import lit.loveletter.LLPlayer;
import oop.lit.model.Action;
import oop.lit.model.PlayerModel;
import oop.lit.model.elements.AbstractGameElement;

/**
 * A game element modelling a love letter card.
 */
public class LLCard extends AbstractGameElement {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private final LLCardType type;
    private final transient BufferedImage image;
    private final transient Action playAction;
    private LLPlayer holdingPlayer;
    /**
     * @param type
     *      the type of the card.
     * @param images
     *      the image manager for this game.
     * @param actionFactory
     *      the action factory for this card.
     */
    public LLCard(final LLCardType type, final LLImageManager images, final LLCardActionFactory actionFactory) {
        super(Optional.of(type.getReadableName()));
        this.type = type;
        this.image = images.getCardImage(this.type);
        this.playAction = actionFactory.getPlayAction(this);
    }

    /**
     * @return
     *      this card type.
     */
    public LLCardType getType() {
        return this.type;
    }

    /**
     * Sets the player holding this card.
     * @param player
     *      the player holding this card.
     */
    public void setHoldingPlayer(final Optional<LLPlayer> player) {
        this.holdingPlayer = player.orElse(null);
    }

    /**
     * @return
     *      the player holding this card.
     */
    public Optional<LLPlayer> getHoldingPlayer() {
        return Optional.ofNullable(this.holdingPlayer);
    }

    /**
     * A method to be performed when the card is discarded.
     */
    public void discarded() {
        if (this.type.equals(LLCardType.PRINCESS)) {
            this.holdingPlayer.knockOut();
        }
        this.holdingPlayer = null;
    }
    @Override
    public List<Action> getActions(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return Arrays.asList(this.playAction);
    }

    @Override
    public Optional<Action> getMainAction(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return Optional.of(this.playAction);
    }

    @Override
    public Optional<BufferedImage> getImage() {
        return Optional.of(this.image);
    }
}
