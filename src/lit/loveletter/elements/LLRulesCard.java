package lit.loveletter.elements;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import lit.loveletter.LLImageManager;
import lit.loveletter.LLImageManager.ImageType;
import oop.lit.model.Action;
import oop.lit.model.PlayerModel;
import oop.lit.model.actions.AbstractAction;
import oop.lit.model.elements.AbstractBoardElement;
import oop.lit.util.IllegalInputException;

/**
 * A board element holding a summary of the rules.
 */
public class LLRulesCard extends AbstractBoardElement {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private final transient BufferedImage image1;
    private final transient BufferedImage image2;
    private final transient Action flipAction;
    private boolean flip;
    /**
     * @param imageManager
     *      the game imageManager.
     */
    public LLRulesCard(final LLImageManager imageManager) {
        super(Optional.of("Rules"), Optional.empty(), Optional.empty(), Optional.empty());
        this.image1 = imageManager.getOtherImage(ImageType.RULES1);
        this.image2 = imageManager.getOtherImage(ImageType.RULES2);
        this.flipAction = new AbstractAction("Flip") {
            @Override
            public void perform() throws IllegalInputException {
                flip = !flip;
                notifyObservers();
            }
        };
    }

    @Override
    public boolean canMove(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return true;
    }
    @Override
    public boolean canScale(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return true;
    }
    @Override
    public boolean canRotate(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return true;
    }

    @Override
    public List<Action> getActions(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return Arrays.asList(flipAction);
    }

    @Override
    public Optional<Action> getMainAction(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return Optional.of(flipAction);
    }

    @Override
    public Optional<BufferedImage> getImage() {
        if (flip) {
            return Optional.of(this.image1);
        } else {
            return Optional.of(this.image2);
        }
    }
}
