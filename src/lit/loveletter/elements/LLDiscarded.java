package lit.loveletter.elements;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import lit.loveletter.LLImageManager;
import lit.loveletter.LLImageManager.ImageType;
import oop.lit.model.Action;
import oop.lit.model.PlayerModel;
import oop.lit.model.actions.AbstractAction;
import oop.lit.model.elements.AbstractBoardElement;
import oop.lit.model.groups.ElementGroup;
import oop.lit.model.groups.GroupViewer;
import oop.lit.util.IllegalInputException;

/**
 * A board element used to show discarded cards.
 */
public class LLDiscarded extends AbstractBoardElement {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private final transient BufferedImage emptyImage;
    private final transient Action showGroup;
    private final ElementGroup<LLCard> group; 
    /**
     * @param group
     *      the group to be visualized.
     * @param imageManager
     *      the game imageManager.
     * @param gViewer
     *      the game group viewer.
     */
    public LLDiscarded(final ElementGroup<LLCard> group, final LLImageManager imageManager, final GroupViewer gViewer) {
        super(group.getGroupName(), Optional.empty(), Optional.empty(), Optional.empty());
        this.group = group;
        this.group.attach(() -> this.notifyObservers());
        this.emptyImage = imageManager.getOtherImage(ImageType.EMPTY_GROUP);
        this.showGroup = new AbstractAction("See group") {
            @Override
            public void perform() throws IllegalInputException {
                gViewer.showGroup(group);
            }
        };
    }

    @Override
    public boolean canMove(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return true;
    }
    @Override
    public boolean canScale(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return true;
    }
    @Override
    public boolean canRotate(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return true;
    }

    @Override
    public List<Action> getActions(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return Arrays.asList(this.showGroup);
    }

    @Override
    public Optional<Action> getMainAction(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return Optional.of(this.showGroup);
    }

    @Override
    public Optional<BufferedImage> getImage() {
        if (this.group.getElements().isEmpty()) {
            return Optional.of(this.emptyImage);
        } else {
            return this.group.getElements().get(this.group.getElements().size() - 1).getImage();
        }
    }

}
