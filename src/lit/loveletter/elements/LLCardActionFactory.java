package lit.loveletter.elements;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import lit.loveletter.LLCardType;
import lit.loveletter.LLPlayer;
import lit.loveletter.LLPlayerHand;
import lit.loveletter.LLPlayerManager;
import oop.lit.model.Action;
import oop.lit.model.PlayerModel;
import oop.lit.model.actions.AbstractAction;
import oop.lit.model.actions.Actions;
import oop.lit.model.groups.ElementGroup;
import oop.lit.util.CollectionsUtils;
import oop.lit.util.IllegalInputException;
import oop.lit.util.InputRequest;
import oop.lit.util.InputRequestsFactory;
import oop.lit.view.ViewRequests;

/**
 * A class used to get actions for a love letter card.
 */
public class LLCardActionFactory {
    private final LLPlayerManager pManager;
    private final ViewRequests view;
    private final ElementGroup<LLCard> deck;
    /**
     * @param pManager
     *      this game player manager.
     * @param view
     *      this game view ViewRequests.
     * @param deck
     *      this game deck.
     */
    public LLCardActionFactory(final LLPlayerManager pManager, final ViewRequests view, final ElementGroup<LLCard> deck) {
        this.pManager = pManager;
        this.view = view;
        this.deck = deck;
    }
    /**
     * @param card
     *      the card you want to get the action for.
     * @return
     *      the play card action.
     *
     * @throws IllegalArgumentException
     *      if the provided card type is null.
     */
    public Action getPlayAction(final LLCard card) {
        switch (card.getType()) {
        case BARON:
            return getBaronAction(card);
        case GUARD:
            return getGuardAction(card);
        case HANDMAID:
            return getHandmaidAction(card);
        case KING:
            return getKingAction(card);
        case PRIEST:
            return getPriestAction(card);
        case PRINCE:
            return getPrinceAction(card);
        case PRINCESS:
        case COUNTESS:
            return getEmptyAction(card);
        default:
            throw new IllegalArgumentException();
        }
    }

    private Action getGuardAction(final LLCard card) {
        return new PlayerTargetAction(card) {
            private Optional<LLPlayer> target;
            private Optional<InputRequest<LLCardType>> cardRequest = Optional.empty();
            private LLCardType type;
            @Override
            public List<InputRequest<?>> getRequests(final InputRequestsFactory irFactory) {
                final List<InputRequest<?>> res = new ArrayList<>(super.getRequests(irFactory));
                if (!res.isEmpty()) {
                    this.cardRequest = Optional.of(irFactory.getChoiceInputRequest("Card",
                            CollectionsUtils.mapWithDifferentStrings(Arrays.asList(LLCardType.values()).stream()
                                    .filter(t -> !t.equals(LLCardType.GUARD)).collect(Collectors.toList()),
                                    LLCardType::getReadableName),
                            Optional.empty()));
                    res.add(cardRequest.get());
                }
                return res;
            }
            @Override
            protected void performCardAction() {
                if (this.target.isPresent() && pManager.getPlayerHand(this.target.get()).get().getElements().get(0)
                        .getType().equals(type)) {
                    this.target.get().knockOut();
                    view.displayMessage(playerString(this.target.get()) + "was knocked out of round.");
                }
            }
            @Override
            protected void checkInputs() throws IllegalInputException {
                this.target = this.getChoiceAndReset();
                if (target.isPresent()) {
                    type = Actions.checkPresentAndGet(cardRequest);
                    this.cardRequest = Optional.empty();
                }
            }
        };
    }
    private Action getPriestAction(final LLCard card) {
        return new PlayerTargetAction(card) {
            private Optional<LLPlayer> target;
            @Override
            protected void performCardAction() {
                if (this.target.isPresent()) {
                    view.displayMessage(playerString(this.target.get()) + "has a " + pManager
                            .getPlayerHand(target.get()).get().getElements().get(0).getType().getReadableName());
                }
            }
            @Override
            protected void checkInputs() throws IllegalInputException {
                this.target = this.getChoiceAndReset();
            }
        };
    }
    private Action getBaronAction(final LLCard card) {
        return new PlayerTargetAction(card) {
            private Optional<LLPlayer> target;
            @Override
            protected void performCardAction() {
                if (target.isPresent()) {
                    final LLCardType myType = pManager.getPlayerHand(pManager.getPlayingPlayer().get()).get().getElements()
                            .get(0).getType();
                    final LLCardType otherType = pManager.getPlayerHand(target.get()).get().getElements().get(0).getType();
                    if (myType.getStrength() > otherType.getStrength()) {
                        target.get().knockOut();
                        view.displayMessage(playerString(this.target.get())
                                + "was knocked out of round by baron, having " + otherType.getReadableName() + ".");
                    } else if (otherType.getStrength() > myType.getStrength()) {
                        pManager.getPlayingPlayer().get().knockOut();
                        view.displayMessage(playerString(pManager.getPlayingPlayer().get())
                                + "was knocked out of round by baron, having " + myType.getReadableName() + ".");
                    }
                }
            }
            @Override
            protected void checkInputs() throws IllegalInputException {
                this.target = this.getChoiceAndReset();
            }
        };
    }
    private Action getKingAction(final LLCard card) {
        return new PlayerTargetAction(card) {
            private Optional<LLPlayer> target;
            @Override
            public boolean canBePerformed() {
                return super.canBePerformed() && checkNoCountess();
            }
            @Override
            protected void performCardAction() {
                if (target.isPresent()) {
                    final LLPlayerHand myHand = pManager.getPlayerHand(pManager.getPlayingPlayer().get()).get();
                    final LLPlayerHand otherHand = pManager.getPlayerHand(this.target.get()).get();
                    final LLCard myCard = myHand.getElements().get(0);
                    final LLCard otherCard = otherHand.getElements().get(0);

                    myHand.removeElement(myCard);
                    otherHand.removeElement(otherCard);

                    myHand.addElement(otherCard);
                    otherCard.setHoldingPlayer(pManager.getPlayingPlayer());

                    otherHand.addElement(myCard);
                    myCard.setHoldingPlayer(this.target);
                }
            }
            @Override
            protected void checkInputs() throws IllegalInputException {
                this.target = this.getChoiceAndReset();
            }
        };
    }
    private Action getPrinceAction(final LLCard card) {
        return new PlayAction(card) {
            private Optional<InputRequest<LLPlayer>> pRequest = Optional.empty();
            private LLPlayer target;
            @Override
            public boolean canBePerformed() {
                return super.canBePerformed() && checkNoCountess();
            }
            @Override
            public List<InputRequest<?>> getRequests(final InputRequestsFactory irFactory) {
                this.checkPerformable();
                pRequest = Optional.of(irFactory.getChoiceInputRequest("Target player",
                        CollectionsUtils.mapWithDifferentStrings(pManager.getAllTargetablePlayers(), LLPlayer::getName),
                        Optional.empty()));
                return Arrays.asList(pRequest.get());
            }
            @Override
            protected void performCardAction() {
                final LLPlayerHand targetHand = pManager.getPlayerHand(target).get();
                final LLCard targetCard = targetHand.getElements().get(0);
                targetHand.removeElement(targetCard);
                pManager.getDiscardedGroup(target).get().addElement(targetCard);
                targetCard.discarded();
                if (deck.getElements().isEmpty()) {
                    target.knockOut();
                } else if (target.canPlay()) {
                    final LLCard deckCard = deck.getElements().get(0);
                    deck.removeElement(deckCard);
                    targetHand.addElement(deckCard);
                    deckCard.setHoldingPlayer(Optional.of(target));
                }
            }
            @Override
            protected void checkInputs() throws IllegalInputException {
                target = Actions.checkPresentAndGet(pRequest);
                pRequest = Optional.empty();
            }
        };
    }

    private boolean checkNoCountess() {
        return pManager.getPlayerHand(pManager.getPlayingPlayer().get()).get().getElements().stream()
                .allMatch(card -> !card.getType().equals(LLCardType.COUNTESS));
    }

    private Action getHandmaidAction(final LLCard card) {
        return new PlayAction(card) {
            @Override
            protected void performCardAction() {
                pManager.getPlayingPlayer().get().setPlayerProtected(true);
            }
            @Override
            protected void checkInputs() throws IllegalInputException {
            }
        };
    }
    private Action getEmptyAction(final LLCard card) {
        return new PlayAction(card) {
            @Override
            protected void performCardAction() {
            }
            @Override
            protected void checkInputs() throws IllegalInputException {
            }
        };
    }

    private String playerString(final PlayerModel p) {
        return "Player " + p.getName() + " ";
    }
    private abstract class PlayAction extends AbstractAction {
        private final LLCard card;

        protected PlayAction(final LLCard card) {
            super("Play card");
            this.card = card;
        }

        @Override
        public boolean canBePerformed() {
            return pManager.getPlayingPlayer().isPresent() && card.getHoldingPlayer().isPresent()
                    && pManager.getActivePlayers().contains(pManager.getPlayingPlayer().get())
                    && pManager.getPlayingPlayer().get().equals(card.getHoldingPlayer().get());
        }

        @Override
        public void perform() throws IllegalInputException {
            this.checkPerformable();
            this.checkInputs();
            pManager.getPlayerHand(pManager.getPlayingPlayer().get()).get().removeElement(card);
            pManager.getDiscardedGroup(pManager.getPlayingPlayer().get()).get().addElement(card);
            this.card.discarded();
            this.performCardAction();
            pManager.nextTurn();
        }

        protected abstract void checkInputs() throws IllegalInputException;
        protected abstract void performCardAction();
    }

    private abstract class PlayerTargetAction extends PlayAction {
        private Optional<InputRequest<LLPlayer>> pRequest = Optional.empty();

        protected PlayerTargetAction(final LLCard card) {
            super(card);
        }
        @Override
        public List<InputRequest<?>> getRequests(final InputRequestsFactory irFactory) {
            this.checkPerformable();
            final Set<LLPlayer> targetablePlayers = pManager.getTargetablePlayersEx();
            if (targetablePlayers.isEmpty()) {
                return Collections.emptyList();
            }
            pRequest = Optional.of(irFactory.getChoiceInputRequest("Target player",
                    CollectionsUtils.mapWithDifferentStrings(targetablePlayers, LLPlayer::getName), Optional.empty()));
            return Arrays.asList(pRequest.get());
        }
        //returns an empty optional only if it is allowed. If the input was not set throws IllegalInputException.
        protected Optional<LLPlayer> getChoiceAndReset() throws IllegalInputException {
            if (!pManager.getTargetablePlayersEx().isEmpty()) {
                final Optional<LLPlayer> res = Optional.of(Actions.checkPresentAndGet(pRequest));
                this.pRequest = Optional.empty();
                return res;
            }
            return Optional.empty();
        }
    }
}
