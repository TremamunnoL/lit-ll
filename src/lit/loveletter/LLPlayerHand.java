package lit.loveletter;

import java.util.List;
import java.util.Optional;

import lit.loveletter.elements.LLCard;
import oop.lit.model.Action;
import oop.lit.model.PlayerModel;
import oop.lit.model.game.Player;
import oop.lit.model.groups.AbstractSelectableElementGroup;

/**
 * A group modelling a love letter player hand.
 */
public class LLPlayerHand extends AbstractSelectableElementGroup<LLCard> {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * @param player
     *      the player this hand is relative to.
     */
    protected LLPlayerHand(final Player player) {
        super(Optional.of(player.getName() + " hand"));
    }

    @Override
    public List<Action> getSelectedActions(final PlayerModel playingPlayer, final List<? extends PlayerModel> turnPlayers) {
        return this.getSingleSelectionActions(playingPlayer, turnPlayers);
    }
}
